# Generated by Django 2.2.16 on 2020-09-17 14:33

from django.db import migrations
import enumfields.fields
import link_all.models


class Migration(migrations.Migration):

    dependencies = [
        ('link_all', '0006_alter_fields_for_button_mixin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='linkbuttonpluginmodel',
            name='link_button_color',
            field=enumfields.fields.EnumField(blank=True, default='primary', enum=link_all.models.ButtonColor, max_length=64, verbose_name='Color'),
        ),
    ]
